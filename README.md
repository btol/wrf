# DTU WRF repository with custom changes

Changes includes:
 - Changes to output dynamical tendencies


## Updating source to new(er) WRF release:
```
git remote add upstream https://github.com/wrf-model/WRF.git
git pull upstream <branch name>
```
